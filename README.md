# Packer

Automatization of a packaging process in shop.

## How does it works

At first, application loads a random transaction and show it to the packer-guy.

After that, packer-guy begins his work, following 7 steps:

Step 1: Pick up a box and scan its barcode (or enter manualy into input form).

Step 2: The status of a transaction is going to change to `open`.

Step 3: Collect products by scanning their barcodes.

Step 4: Close the box and scan its barcode again.

Step 5. The status of a transaction is going to change to `closed`.

Step 6. Application will generate pdf label for the transaction and save it in `web/pdfs` dir.

Step 7. Finished.


## About

This is part of a project I created, working at X-Coding.

I present it as a sample code in: PHP / Symfony2 / JavaScript.

All rights reserved X-Coding.pl.


## How to install

* Download the project

        git clone git@bitbucket.org:kjm/packer.git

        cd packer



* Permissions



* Create new MySQL database, e.g:

        CREATE SCHEMA `packer` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;



* Install [composer](https://github.com/composer/composer)



* Run composer to install project packages

        composer install



* Create tables and seeds

        php app/console doctrine:schema:create

        php app/console doctrine:fixtures:load



* Compile assets

        php app/console assets:install web



* Run the server

        php app/console server:start



* Run the application

        http://localhost:8000



## Dependencies

* MySQL

* [Wkhtmltopdf](http://wkhtmltopdf.org/)

## Copyrights

X-Coding.pl
