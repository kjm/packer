<?php

namespace Xc\PackerBundle\Services;

use Xc\PackerBundle\Entity\Transaction;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpKernel\Kernel;
use Knp\Bundle\SnappyBundle\Snappy\LoggableGenerator;
use BG\BarcodeBundle\Util\Base1DBarcode;

/**
 * LabelManager
 */
class LabelManager
{
    const TEMP_DIR  = 'web/pdfs/temp/';
    const PRINT_DIR = 'web/pdfs/print/';
    
    private $pdf;
    private $templating;
    private $kernel;
    private $transaction;
    private $tempPath;
    private $printPath;
    
    public function __construct(LoggableGenerator $pdf, EngineInterface $templating, Kernel $kernel)
    {
        $this->pdf = $pdf;
        $this->templating = $templating;
        $this->kernel = $kernel;
    }
    
    public function initialize(Transaction $transaction)
    {
        $this->transaction = $this->tempPath = $this->printPath = $this->address = null;
        $this->transaction = $transaction;
        
        $this->address = $this->transaction->getShippingAddress();
        if(!is_object($this->address)){
            throw new \Exception('Transakcja nie posiada adresu wysyłki');
        }
        
        $root = str_replace('app', '', $this->kernel->getRootDir());
        $this->tempPath  = $root . self::TEMP_DIR . $this->transaction->getNumberWithHash() . '.pdf';
        $this->printPath = $root . self::PRINT_DIR . $this->transaction->getNumberWithHash() . '.pdf';
        
        $this->createDirIfNescessery($root . self::PRINT_DIR);
    }
    
    /**
     *
     */
    public function generateTempPDF()
    {
        if(!is_object($this->transaction) || !$this->tempPath || !$this->printPath){
            throw new \Exception('Nie wybrano numeru etykiety, lub podany numer etykiety nie istnieje');
        }
        
        if(!is_object($this->address)){
            throw new \Exception('Transakcja nie ma adresu wysylki');
        }
        
        if(!$this->PDFExistsInTempDir()){
            $this->createPDF();
        }

        return true;
    }
    
    /**
     * Creates PDF if needed and copies it from temp to print directory.
     */
    public function printPDF()
    {
        if($this->PDFExistsInPrintDir()){
            throw new \Exception('Etykieta oczekuje w kolejce na wydrukowanie');
        }
        
        $this->generateTempPDF();        
        $this->copyToPrintDir();
        
        return true;
    }
    
    
    /**
     * Creates pdf in temp directory.
     */
    private function createPDF()
    {
        $myBarcode = new Base1DBarcode();  
        $number = $this->transaction->getNumber();
        $barcodeHtml = $myBarcode->getBarcodeHTML($number, 'I25', 3, 80);
        
        $this->pdf->generateFromHtml(
            $this->templating->render(
            'XcPackerBundle:Transaction:label.html.twig',
                array(
                    'shippingCode' => $this->shippingCode,
                    'transaction'  => $this->transaction,
                    'address'      => $this->address,
                    'barcode'      => $barcodeHtml,
                    'number'       => $number
                )
            ),
            $this->tempPath,
            array(
                'page-width' => '10cm',
                'page-height' => '10cm', 
                'margin-top' => '0.3cm',
                'margin-right' => '0.3cm',
                'margin-bottom' => '0.3cm',
                'margin-left' => '0.23cm'
            )
        );
        umask(0);
        chmod($this->tempPath, 0777);
    }
    
    /**
     * Checks if pdf file exists in temp directory.
     */
    private function PDFExistsInTempDir()
    {
        return file_exists($this->tempPath);
    }
    
    /**
     * Checks if pdf file exists in print directory.
     */
    private function PDFExistsInPrintDir()
    {
        return file_exists($this->printPath);
    }
    
    /**
     * Creates pdf in temp directory.
     */
    private function copyToPrintDir()
    {
        if(copy($this->tempPath, $this->printPath)){
            umask(0);
            chmod($this->printPath, 0777); 
        }else{
            throw new \Exception('Blad kopiowania');
        }
    }
    
    private function createDirIfNescessery($dir)
    {
        if(!file_exists($dir)){
            umask(0);
            if(mkdir($dir, 0777, true)){
                chmod($dir, 0777);
            }else{
                throw new \Exception('Blad podczas tworzenia katalogu');
            }
        }
    }
}
