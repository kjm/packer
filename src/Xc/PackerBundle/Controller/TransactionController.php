<?php

namespace Xc\PackerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class TransactionController extends BaseController
{
    public function updateAction($id, Request $request)
    {
        try{
          $transaction = $this->entityManager
                  ->getRepository('XcPackerBundle:Transaction')
                  ->find($id);

          if(!is_object($transaction)){
            throw new \Exception('Brak transakcji');
          }
          
          $status = $request->request->get('status');

          $transaction->setStatus($status);
          $transaction->setUpdated();

          $this->entityManager->persist($transaction);
          $this->entityManager->flush();

          $result = array('msg' => 'Pomyslnie zapisano');
          return $this->jsonSuccess(array($result));
    
        } catch (\Exception $exception) {
            $error = array('code' => $exception->getCode(), 'message' => $exception->getMessage());

            return $this->jsonFail($error);
        }
    }

    public function printAction($id)
    {
        try{
            $transaction = $this->entityManager
                  ->getRepository('XcPackerBundle:Transaction')
                  ->find($id);

            if(!is_object($transaction)){
              throw new \Exception('Brak transakcji');
            }
            
            $labelManager = $this->get('packer.label_manager');
            $labelManager->initialize($transaction);
            $labelManager->printPDF();
        
            $result = array('msg' => 'Pomyslnie wygenerowano i wysłano żądanie wydruku');
            return $this->jsonSuccess(array($result));
        
        } catch (\Exception $exception) {
            $error = array('code' => $exception->getCode(), 'message' => $exception->getMessage());

            return $this->jsonFail($error);
        }
    }
}
