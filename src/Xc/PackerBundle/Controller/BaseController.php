<?php

namespace Xc\PackerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManager;

class BaseController extends Controller
{
    protected $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    protected function jsonSuccess($data)
    {
        $return = array('success' => true, 'data' => $data, 'error' => array());
        
        return new JsonResponse(array($return), Response::HTTP_OK, array('content-type'=>'application/json'));
    }
    
    protected function jsonFail($error)
    {
        $return = array('success' => false, 'data' => array(), 'error' => $error);
        
        return new JsonResponse(array($return), Response::HTTP_UNPROCESSABLE_ENTITY, array('content-type'=>'application/json'));
    }
}
