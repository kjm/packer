<?php

namespace Xc\PackerBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Doctrine\ORM\EntityManager;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Xc\PackerBundle\Entity\Transaction;

class HomeController extends Controller
{
    private $entityManager;
    private $templating;
    private $transactionRepository;

    public function __construct(EntityManager $entityManager, EngineInterface $templating)
    {
        $this->entityManager = $entityManager;
        $this->templating = $templating;
        $this->transactionRepository = $this->entityManager->getRepository('XcPackerBundle:Transaction');
    }
    
    public function indexAction()
    {
        $randomTransaction = $this->transactionRepository->findRandom();
        if(!is_object($randomTransaction)){
            throw new \Exception('Brak transakcji');
        }
        
        $randomTransactionId = $randomTransaction->getId();
        $transaction = $this->transactionRepository->findIncludingProducts($randomTransactionId);
        
        $boxCodes = array('0000000000000', '1111111111111');
        
        $products = [];
        $tps = $transaction->getTransactionProducts();
       
        foreach($tps as $key => $tp){
          $products[$key]['id'] = $tp->getProduct()->getId();
          $products[$key]['barcode1'] = $tp->getProduct()->getBarcode1();
          $products[$key]['barcode2'] = $tp->getProduct()->getBarcode2();
          $products[$key]['title'] = $tp->getProduct()->getTitle();
          $products[$key]['symbol'] = $tp->getProduct()->getSymbol();
          $products[$key]['qty'] = $tp->getQty();
        }
        
        return $this->templating->renderResponse('XcPackerBundle:Transaction:index.html.twig', array(
            'transaction' => $transaction,
            'boxCodesJson' => json_encode($boxCodes),
            'productsJson' => json_encode($products),
            'statusStart' => Transaction::STATUS_DURING_PACKING,
            'statusEnd' => Transaction::STATUS_PACKED,
        ));
    }
}
