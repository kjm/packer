<?php

namespace Xc\PackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Xc\PackerBundle\Entity\ShippingAddress;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Transaction
 */
class Transaction
{
    const STATUS_NEW = 1;
    const STATUS_PENDING = 2;
    const STATUS_FINISHED = 3;
    const STATUS_RETURNED = 4;
    const STATUS_CANCELLED = 5;
    const STATUS_PACKED = 6;
    const STATUS_TO_PACK = 7;
    const STATUS_COMPLETED = 8;
    const STATUS_DELAYED = 9;
    const STATUS_AMAZON_SHIPPING = 10;
    const STATUS_DURING_COMPLETATION = 11;
    const STATUS_DURING_PACKING = 12;
    
    private $id;
    private $status;
    private $number;
    private $created;
    private $updated;
    private $shippingAddress;
    private $transactionProducts;
    
    private $numberWithHash;

    public function getId()
    {
        return $this->id;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }
    
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    public function getNumber()
    {
        return$this->number;
    }
    
    public function setCreated()
    {
        $this->created = new \DateTime();
        $this->setUpdated();

        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }
    
    public function setUpdated()
    {
        $this->updated = new \DateTime();

        return $this;
    }

    public function getUpdated()
    {
        return $this->updated;
    }
    
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    public function setShippingAddress(ShippingAddress $shippingAddress)
    {
        $this->shippingAddress = $shippingAddress;
        
        return $this;
    }
    
    public function setTransactionProducts(ArrayCollection $transactionProducts)
    {
        $this->transactionProducts = $transactionProducts;
        
        return $this;
    }
    
    public function getTransactionProducts()
    {
        return $this->transactionProducts;
    }
    
    public function getNumberWithHash()
    {
        return $this->numberWithHash ? $this->numberWithHash : $this->number . '_' . md5($this->number . $this->created->getTimestamp() . '3%75gdgA1h!K');
    }
}
