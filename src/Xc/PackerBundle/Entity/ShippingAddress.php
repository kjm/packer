<?php

namespace Xc\PackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShippingAddress
 */
class ShippingAddress
{
    private $id;
    private $street;
    private $number;
    private $postCode;
    private $city;
    private $country;
    private $phone;
    private $name;
    private $surname;
    private $created;
    private $updated;

    public function getId()
    {
        return $this->id;
    }

    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;

        return $this;
    }

    public function getPostCode()
    {
        return $this->postCode;
    }

    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    public function getSurname()
    {
        return $this->surname;
    }
    
    public function setCreated()
    {
        $this->created = new \DateTime();
        $this->setUpdated();

        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }
    
    public function setUpdated()
    {
        $this->updated = new \DateTime();

        return $this;
    }

    public function getUpdated()
    {
        return $this->updated;
    }
}
