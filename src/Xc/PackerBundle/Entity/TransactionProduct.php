<?php

namespace Xc\PackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Xc\PackerBundle\Entity\Product;
use Xc\PackerBundle\Entity\Transaction;

/**
 * TransactionProduct
 */
class TransactionProduct
{
    private $id;
    private $qty;
    private $product;
    private $transaction;
    
    public function getId()
    {
        return $this->id;
    }

    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    public function getQty()
    {
        return $this->qty;
    }
    
    public function setProduct(Product $product)
    {
        $this->product = $product;
        
        return $this;
    }
    
    public function getProduct()
    {
        return $this->product;
    }
    
    public function setTransaction(Transaction $transaction)
    {
        $this->transaction = $transaction;
        
        return $this;
    }
    
    public function getTransaction()
    {
        return $this->transaction;
    }
}
