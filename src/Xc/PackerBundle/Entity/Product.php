<?php

namespace Xc\PackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Product
 */
class Product
{
    private $id;
    private $barcode1;
    private $barcode2;
    private $title;
    private $symbol;
    private $transactionProducts;

    public function getId()
    {
        return $this->id;
    }

    public function setBarcode1($barcode1)
    {
        $this->barcode1 = $barcode1;

        return $this;
    }

    public function getBarcode1()
    {
        return $this->barcode1;
    }

    public function setBarcode2($barcode2)
    {
        $this->barcode2 = $barcode2;

        return $this;
    }

    public function getBarcode2()
    {
        return $this->barcode2;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getSymbol()
    {
        return $this->symbol;
    }
    
    public function setTransactionProducts(ArrayCollection $transactionProducts)
    {
        $this->transactionProducts = $transactionProducts;
        
        return $this;
    }
    
    public function getTransactionProducts()
    {
        return $this->transactionProducts;
    }
}
