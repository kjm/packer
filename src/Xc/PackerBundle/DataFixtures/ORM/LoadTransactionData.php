<?php

namespace Xc\PackerBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Xc\PackerBundle\Entity\Transaction;
use Xc\PackerBundle\Entity\ShippingAddress;
use Doctrine\Common\Collections\ArrayCollection;

class LoadTransactionAndShippingAddressData extends AbstractFixture implements OrderedFixtureInterface
{
    const ITEMS = 100;
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $addresses = new ArrayCollection();
        
        for($i = 0; $i < self::ITEMS; $i++){
            $address = new ShippingAddress();
            $address->setStreet('Street');
            $address->setNumber(rand(1,100));
            $address->setPostCode(rand(1,99) . '-' . rand(1,999));
            $address->setCity('Berlin');
            $address->setCountry('DE');
            $address->setPhone('111222333');
            $address->setName('Thomas');
            $address->setSurname('Stefan');
            $address->setCreated();
            
            $addresses->add($address);
            $manager->persist($address);
        }
        
        $manager->flush();
        
        for($i = 0; $i < self::ITEMS; $i++){
            $transaction = new Transaction();
            $transaction->setStatus(rand(Transaction::STATUS_NEW, Transaction::STATUS_DURING_PACKING));
            $transaction->setNumber('123123123' . rand(100, 999));
            $transaction->setCreated();
            $transaction->setShippingAddress($addresses->get($i));
            
            $manager->persist($transaction);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}