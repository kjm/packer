<?php

namespace Xc\PackerBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Xc\PackerBundle\Entity\TransactionProduct;

class LoadTransactionProductData extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $transactions = $manager->getRepository('XcPackerBundle:Transaction')->findAll();
        $products = $manager->getRepository('XcPackerBundle:Product')->findAll();
        $productCount = $manager->getRepository('XcPackerBundle:Product')->count();
        
        foreach($transactions as $transaction){
            
            for($i = 0; $i < rand(1, 5); $i++){
                $transactionProduct = new TransactionProduct();
                $transactionProduct->setTransaction($transaction);
                $transactionProduct->setProduct($products[rand(1, $productCount-1)]);
                $transactionProduct->setQty(rand(1, 10));

                $manager->persist($transactionProduct);
            }
        }
        
        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}