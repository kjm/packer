<?php

namespace Xc\PackerBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Xc\PackerBundle\Entity\Product;

class LoadProductData extends AbstractFixture implements OrderedFixtureInterface
{
    const ITEMS = 1000;
    
    private $barcode = '1000000000000';
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        for($i = 0; $i < self::ITEMS; $i++){
            $product = new Product();
            $product->setBarcode1($this->getNextBarcode());
            $product->setBarcode2($this->getNextBarcode());
            $product->setTitle('Product #' . rand(1, 10000));
            $product->setSymbol('AB' . sprintf('%010d', rand(1, 10000)));
            
            $manager->persist($product);
        }
        
        $manager->flush();
    }
    
    private function getNextBarcode()
    {
        return $this->barcode = bcadd($this->barcode, 1); 
    }

    public function getOrder()
    {
        return 1;
    }
}