var CodeValidator = function () {
    this.lastCode = null;
};

CodeValidator.prototype.validate = function (code, errorCallback, validCallback){

    if (code === undefined || code === '') {
        return;
    }

    if (code === this.lastCode) {
        return;
    }
    this.lastCode = code;

    if (code.match(/[^0-9]/i) !== null) {
        errorCallback('kod zawiera niepoprawny znak. Dozwolone tylko cyfry...');
        return;
    }

    if (code.length < 13) {
        errorCallback('za mało cyf...');
        return;
    }

    if (code.length > 13) {
        errorCallback('za dużo cyfr...');
        return;
    }

    validCallback(code);

    return;
};

CodeValidator.prototype.reset = function () {
    this.lastCode = null;
    
    return;
};
