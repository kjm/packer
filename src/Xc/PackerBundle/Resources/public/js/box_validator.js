var BoxValidator = function (validCodes) {
    this.validCodes = validCodes;
    this.lastCode = null;
};

BoxValidator.prototype.validate = function (code, errorCallback, validCallback) {
    if (code === this.lastCode) {
        return;
    }
    this.lastCode = code;

    if (this.validCodes.indexOf(code) === -1) {
        errorCallback('nie ma w bazie boxa o takim numerze...');
    } else {
        validCallback(code);
    }

    return;
};

BoxValidator.prototype.validateComparingWithLast = function (code, errorCallback, validCallback) {
    if (this.lastCode === code){
        validCallback(code);
    } else {
        errorCallback('kod inny niż kod otwarcia boxa...');
    }

    return;
};
