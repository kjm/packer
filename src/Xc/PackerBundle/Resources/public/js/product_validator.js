var ProductValidator = function (products) {

    var i;
    this.productsLeft = 0;
    this.products = products;

    for (i = 0; i < this.products.length; i += 1) {
        this.products[i].scanned = 0;
        this.productsLeft += this.products[i].qty;
    }
};

ProductValidator.prototype.validate = function (code, errorCallback, validCallback) {

    var i;
    for (i = 0; i < this.products.length; i += 1) {
        if (this.products[i].barcode1 === code || this.products[i].barcode2 === code) {
            if (this.products[i].scanned < this.products[i].qty) {
                this.products[i].scanned += 1;
                this.productsLeft -= 1;
                validCallback(code);

                return;
            }
        }
    }

    errorCallback('nie ma produktu o takim numerze...');
    return;
};

ProductValidator.prototype.getProducts = function (callback) {
    var i;
    for (i = 0; i < this.products.length; i += 1) {
        callback(this.products[i]);
    }
};

ProductValidator.prototype.anyProductsLeft = function () {
    return this.productsLeft !== 0;
};
