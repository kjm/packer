var packerModule = function (config) {

    var id = config.id,
        transactionId = config.transactionId,
        statusBlocked = config.statusBlocked,
        statusFinished = config.statusFinished,
        logSelector = config.logSelector,
        inputSelector = config.inputSelector,
        productsSelector = config.productsSelector,
        state = 0;

    var Packer = function () {
        this.codeValidator = new CodeValidator();
        this.boxValidator = new BoxValidator(config.boxCodesJson);
        this.productValidator = new ProductValidator(config.productsJson);
    };

    Packer.prototype.getId = function () {
        return id;
    };

    Packer.prototype.getTransactionId = function () {
        return transactionId;
    };

    Packer.prototype.getStatusBlocked = function () {
        return statusBlocked;
    };

    Packer.prototype.getStatusFinished = function () {
        return statusFinished;
    };

    Packer.prototype.getState = function () {
        return state;
    };

    Packer.prototype.nextState = function () {
        state += 1;
        this.moveActiveClass();

        return state;
    };

    Packer.prototype.showError = function (msg) {
        console.log(msg);
        logSelector.find('span').text(msg);
        logSelector.find('span').removeClass('grey');
        logSelector.find('span').addClass('red');
        return;
    };

    Packer.prototype.showMessage = function (msg) {
        console.log(msg);
        logSelector.find('span').text(msg);
        logSelector.find('span').removeClass('red');
        logSelector.find('span').addClass('grey');
    };

    Packer.prototype.showMessageIfEmpty = function (msg) {
        console.log(msg);
        if (logSelector.find('span').text().length === 0) {
            this.showMessage(msg);
        }
    };

    Packer.prototype.moveActiveClass = function () {
        logSelector.removeClass('active');
        logSelector = logSelector.next();
        logSelector.addClass('active');
    };

    Packer.prototype.reloadProducts = function () {
        productsSelector.text('');
        this.productValidator.getProducts(function (product) {
            productsSelector.append('<li>' + product.title + ': <b>(' + product.scanned + '/' + product.qty + ')</b></li> ');
        });
    };

    Packer.prototype.clearInput = function () {
        inputSelector.val('');
        inputSelector.prop('disabled', false);
        inputSelector.focus();
    };

    Packer.prototype.disableInput = function () {
        inputSelector.prop('disabled', true);
    };

    return new Packer();
};