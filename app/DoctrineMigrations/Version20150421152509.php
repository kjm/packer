<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150421152509 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE packer.transaction_products (id INT AUTO_INCREMENT NOT NULL, product_id INT NOT NULL, transaction_id INT NOT NULL, qty INT NOT NULL, INDEX IDX_75CD2A8E4584665A (product_id), INDEX IDX_75CD2A8E2FC0CB0F (transaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE packer.products (id INT AUTO_INCREMENT NOT NULL, barcode1 VARCHAR(13) NOT NULL, barcode2 VARCHAR(13) NOT NULL, title VARCHAR(255) NOT NULL, symbol VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE packer.transaction_products ADD CONSTRAINT FK_75CD2A8E4584665A FOREIGN KEY (product_id) REFERENCES packer.products (id)');
        $this->addSql('ALTER TABLE packer.transaction_products ADD CONSTRAINT FK_75CD2A8E2FC0CB0F FOREIGN KEY (transaction_id) REFERENCES packer.transactions (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE packer.transaction_products');
        $this->addSql('DROP TABLE packer.products');
    }
}
